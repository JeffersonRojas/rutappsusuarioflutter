import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget{

  LoginButton({
    this.btnColor,
    this.icono,
    this.texto,
    this.onClick
  });

  final Color btnColor;
  final IconData icono;
  final String texto;
  final ValueChanged<int> onClick;


  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 203.0,
      height: 33.0,
      margin: EdgeInsets.only(bottom: 5.0),
      padding: EdgeInsets.all(1.0),
      decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(6.0),
          color: Colors.transparent,
          border: new Border.all(
              width: 1.0,
              color: btnColor
          )
      ),
      child: new RaisedButton.icon(
          onPressed: () => onClick(1),
          icon: Icon(
              icono,
              color: btnColor,
              size: 27.0),
          label: Text(
              texto,
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold
              )
          ),
          color: Colors.white,
          splashColor: btnColor,
          textColor: btnColor
      ),
    );
  }
}