import 'package:flutter/material.dart';
import 'package:rutappsusuario/widgets/login/btnLogin.dart';
import 'package:rutappsusuario/widgets/login/dots.dart';
import 'package:rutappsusuario/widgets/login/page1.dart';
import 'package:rutappsusuario/widgets/login/page2.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:rutappsusuario/util/assets.dart';

//import './Page3.dart';

class _LoginMainPageState extends State<LoginMainPage> {
  final _controller = new PageController();
  final List<Widget> _pages = [
    Page1(),
    Page2(),
    //Page3(),
  ];
  int page = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isDone = page == _pages.length - 1;
    return new Scaffold(
        backgroundColor: Colors.white,
        body: new LoginPanel()
    );
  }
}

class LoginMainPage extends StatefulWidget {
  LoginMainPage({Key key}) : super(key: key);

  @override
  _LoginMainPageState createState() => new _LoginMainPageState();
}

class _LoginPaneState extends State<LoginPanel> {
  final _controller = new PageController();
  final List<Widget> _pages = [
    Page1(),
    Page2(),
    //Page3(),
  ];
  int page = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new SafeArea(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            width: MediaQuery.of(context).size.width * 0.5,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: new Image(
              image: AssetImage(Assets.loginLogo),
              fit: BoxFit.fitWidth,
            ),
          ),

        new Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(bottom: 150),
              child:new Image(
                image: AssetImage(Assets.loginBg),
                fit: BoxFit.fitWidth,
              ),
            ),
            new Positioned.fill(
                child: new PageView.builder(
                  physics: new AlwaysScrollableScrollPhysics(),
                  controller: _controller,
                  itemCount: _pages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _pages[index % _pages.length];
                  },
                  onPageChanged: (int p) {
                    setState(() {
                      page = p;
                    });
                  },
                ),
            ),
          ],
        ),
        //new Padding(padding: EdgeInsets.only(bottom: 150.0)),
          new Padding(
            padding: const EdgeInsets.all(1.0),
            child: new DotsIndicator(
              controller: _controller,
              itemCount: _pages.length,
              onPageSelected: (int page) {
                _controller.animateToPage(
                  page,
                  duration: const Duration(milliseconds: 4800),
                  curve: Curves.ease,
                );
              },
            ),
          ),
        new Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new LoginButton(
                btnColor: Colors.blue,
                icono: MdiIcons.facebookBox,
                texto: "Inicia sesión",
                onClick: (int a) {
                  fbLogin();
                }),
            new LoginButton(
                btnColor: Colors.red,
                icono: MdiIcons.googlePlusBox,
                texto: "Inicia sesión",
                onClick: (int a) {
                  googleLogin();
                }),
          ],
        )
      ],
    ));
  }
}

class LoginPanel extends StatefulWidget {
  LoginPanel({Key key}) : super(key: key);

  @override
  _LoginPaneState createState() => new _LoginPaneState();
}

void fbLogin() {
  print("En Facebook");
}

//final GoogleSignIn _googleSignIn = GoogleSignIn();
final GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);
final FirebaseAuth _auth = FirebaseAuth.instance;

/*
Future<FirebaseUser> _handleSignIn() async {
  print("XXX---1");
  GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  print("XXX---2");
  GoogleSignInAuthentication googleAuth = await googleUser.authentication;
  print("XXX---3");
  FirebaseUser user = await _auth.signInWithGoogle(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  print("signed in " + user.displayName);
  return user;
}
*/

Future<void> _handleSignIn() async {
  try {
    print("Logueate");
    await _googleSignIn.signIn();
  } catch (error) {
    print(error);
  }
}

void googleLogin() {
  /*
  _handleSignIn()
      .then((FirebaseUser user){
        print("Logueado");
        print(user);
      })
      .catchError((e) {
        print("error");
        print(e);
      });
      */

  _handleSignIn();
}
